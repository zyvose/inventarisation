﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inventarisation.Models;

namespace Inventarisation.Controllers
{
    public class HomeController : Controller
    {
        
        
        DB_InventarisationEntities db = new DB_InventarisationEntities();
        
        
        public ActionResult Index()
        {

            
            IEnumerable<Employee> employees = db.Employees;
            
            ViewBag.Employees = employees;
            
            
            
            return View();
        }
        [HttpGet]
        public ActionResult Add_Employee(int id)
        {
            
            ViewBag.Employee_ID = id+1;
            return View();
        }
        [HttpPost]
        public ActionResult Add_Employee(Employee employee)
        {

            if (ModelState.IsValid)
            {
                db.Employees.Add(employee);

                try
                { db.SaveChanges(); }
                catch (System.Data.Entity.Infrastructure.DbUpdateException e)
                {
                    return Redirect("/Home/Error");
                }
                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/Error");
            }
            
        }
        [HttpGet]
        public ActionResult Delete(int id)
        {
            ViewBag.Worker = db.Employees.Find(id);
            return View();
        }
        
        [HttpPost]
        public ActionResult Delete(Employee employee, string action)
        {
            if (action== "delete")
            {
                db.Employees.Remove(db.Employees.Find(employee.Employee_ID));


                db.SaveChanges();
            }
            return Redirect("/Home/Index");
        }
        [HttpGet]
        public ActionResult Change(int id)
        {
            ViewBag.Worker = db.Employees.Find(id);
            return View();
        }
        [HttpPost]
        public ActionResult Change(Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Employees.Find(employee.Employee_ID).Name = employee.Name;
                db.Employees.Find(employee.Employee_ID).Surname = employee.Surname;
                db.Employees.Find(employee.Employee_ID).Patronymic = employee.Patronymic;
                db.Employees.Find(employee.Employee_ID).Date_of_birth = employee.Date_of_birth;
                db.Employees.Find(employee.Employee_ID).Position = employee.Position;
                db.Employees.Find(employee.Employee_ID).Role = employee.Role;


                try
                { db.SaveChanges(); }
                catch (System.Data.Entity.Infrastructure.DbUpdateException e)
                {
                    return Redirect("/Home/Error");
                }
                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/Error");
            }
        }
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Error(string str)
        {

            return Redirect("/Home/Index");
        }
    }
}